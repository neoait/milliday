fn main() {
    use chrono::prelude::*;
    let utc: DateTime<Utc> = Utc::now();
    let mut hour: u32 = utc.format("%H").to_string().parse().unwrap();
    let mut minute: u32 = utc.format("%M").to_string().parse().unwrap();
    let second: f32 = utc.format("%S").to_string().parse().unwrap();
    hour += 1;
    hour *= 3600;
    minute *= 60;
    let mut milliday: f32 = hour as f32 + minute as f32 + second as f32;
    milliday /= 86.4;
    println!("{}", milliday as u32);
}
