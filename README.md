# Milliday/.beat/Internet Time

A simple program to show an alternate time system on your terminal.
The program has no options or anything and is very simple.

## Dependencies
* rust
* [chrono](https://github.com/chronotope/chrono)

## Building on linux
Install rust from your package manager

    $ git clone https://gitlab.com/neoait/milliday.git milliday
    $ cd milliday
    $ cargo fetch
    $ cargo build --release

After building, the binary should be in `target/release/milliday`. Just copy it to a $PATH env path. eg `sudo cp target/release/milliday /bin/milliday`.
